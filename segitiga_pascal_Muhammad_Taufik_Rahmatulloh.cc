#include <iostream>

using namespace std;

// Fungsi untuk menghitung factorial 
int factorial(int n){
    if (n <= 1)
    return 1;
    else
    return n * factorial(n -1);
}

int main() {
    int N;
    cout << "Masukkan N: ";
    cin >> N;
    
    //Membuat pola
    for (int i = 0; i < N; i++){
        for (int j = 0;j <= i; j++){
        int coef = factorial(i) / (factorial(j)* factorial(i-j));
        cout << coef << "";
        }
        cout << endl;
    }
   
    return 0;
}
